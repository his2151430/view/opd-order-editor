import { Injectable, WritableSignal, inject } from '@angular/core';
import { Coding } from '@his-base/datatypes/dist';
import { JetStreamWsService } from '@his-base/jetstream-ws';
import { SharedService } from '@his-base/shared';
import { DrugOrder, KeyOptions, RouteOption, MedFreqTime, SearchReq, Item} from '@his-viewmodel/med-order/dist';
import { DrugOrderRowStatus, OpdDrugOrderStatus } from './opd-drug-order.enum';

@Injectable({
  providedIn: 'root'
})
export class DrugOrderEditorService {
  selectedId!: string;
  #drugOrders!: WritableSignal<DrugOrder[]>;
  #initDrugOrders!: DrugOrder[];
  #medFreqTime!: MedFreqTime[];
  #updateTime!: Date;

  //

  #searchReq: SearchReq;
  #freqTimes: KeyOptions = {};
  #routes: RouteOption = {};
  // #options: KeyOptions;

  #jetStreamWsService = inject(JetStreamWsService);
  #sharedService = inject(SharedService);

  constructor() {
    this.#searchReq = {
      branch: this.#sharedService.appPortal?.branch as string,
      date: this.#sharedService.appPortal?.orderTime as Date,
    };
  }

  /** 取得使用頻率
   * @param medType
   * @return Coding[]
   */
  async getFreqTimes(medType: Coding): Promise<Coding[]> {

    if (!this.#freqTimes[medType.code]) {
      this.#freqTimes[medType.code] = await this.#getMedFreqTimes(medType);
    }
    return this.#freqTimes[medType.code];
  }

  /** 將Promise轉成Object
   * @param medType
   * @return Object
   */
  async #getMedFreqTimes(medType: Coding): Promise<Coding[]> {

    const searchReq = { ...this.#searchReq, keys: [medType.code] };
    const medFreqTimes = await this.#reqMedFreqTimes(searchReq);

    return medFreqTimes[medType.code];
  }

  /** 取得使用頻率
   *  @param medType
   */
  async #reqMedFreqTimes(searchReq: SearchReq): Promise<KeyOptions> {

    return await this.#jetStreamWsService.request('medOrder.medFreqTimes.find', searchReq);
  }

  /** 取得使用途徑
   * @param medType
   * @return Coding[]
   */
  async getMedRoutes(medType: Coding): Promise<Coding[]> {

    if (!this.#routes[medType.code]) {

      console.log( await this.#getMedRoutes(medType));

      this.#routes[medType.code] = await this.#getMedRoutes(medType);
    }

    return this.#routes[medType.code];
  }

  /** 將Promise轉成Object
   * @param medType
   * @return Object
   */
  async #getMedRoutes(medType: Coding): Promise<Item[]> {

    const searchReq = {...this.#searchReq, keys: [medType.code]};
    const medRoutes = await this.#reqMedRoutes(searchReq);

    console.log(await this.#reqMedRoutes(searchReq));

    return medRoutes[medType.code];
  }

  /** 取得使用途徑
   *  @param medType
   */
  async #reqMedRoutes(searchReq: SearchReq): Promise<RouteOption> {

    return await this.#jetStreamWsService.request('medOrder.medRoutes.find', searchReq);
  }

  //


  setDrugOrders(drugOrders: WritableSignal<DrugOrder[]>, initDrugOrders: DrugOrder[]) {
    this.#drugOrders = drugOrders;
    this.#initDrugOrders = initDrugOrders;
  }

  selectRow(_id: string) {
    this.selectedId = _id;
  }

  async initial(updateTime: Date) {

    this.#updateTime = updateTime;
    // this.#medFreqTime = await this.#getMedFreqTime();
  }

  getUnitMenu() {
    const units = ['Tab', 'Vial'];
    return [{ items: this.#getUnitItem(units) }];
  }

  getNumberMenu() {
    const useDays = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 28];
    return useDays.map((x) => ({ label: x.toString(), command: () => this.#onUpdateDays(x) }));
  }

  onClickNew() {
    const target = document.querySelector('.hpc-side-menu-prescription');
    if (!target) return;

    const clickEvent = new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window,
    });
    const ariaExpanded = target.getAttribute('aria-expanded');
    if (ariaExpanded === 'true') return;

    setTimeout(() => {
      target.dispatchEvent(clickEvent);
    }, 165);
  }

  handleValueChanged(coding: Coding, selectedType: string, selectedId: string) {

    if (selectedType === 'route') {
      this.updateDrugOrder(selectedId, { key: 'route', value: coding });
    }

    if (selectedType === 'frequency') {
      this.updateDrugOrder(selectedId, { key: 'frequency', value: coding });
      const index = this.#drugOrders().findIndex((x) => x._id === selectedId);

      this.setTotalQty(this.#drugOrders()[index], index);
    }
    this.appendOriginalOrder(selectedId);
  }

  setTotalQty(drugOrder: DrugOrder, index: number) {

    if (!drugOrder) return;

    const totalQty = this.#getTotalQty(drugOrder); // 根據劑量、頻率、天數計算出總量

    if (!totalQty) return;

    this.updateDrugOrder(this.selectedId, { key: 'total', value: totalQty });

    // const order = { ...drugOrder,total: totalQty };
    // // order.total.value = totalQty;
    // this.#drugOrders.update(orders =>
    //   orders.map((x, y) => {
    //     if (y === index) return order;

    //     return x;
    //   }));
  }

  getStatus(index: number, drugOrder: DrugOrder, initDrugOrders: DrugOrder[]): DrugOrderRowStatus {
    if (drugOrder.status.code === OpdDrugOrderStatus.invalid) return DrugOrderRowStatus.deleted;
    if (index > (initDrugOrders.length - 1)) return DrugOrderRowStatus.new;
    if (this.#isOrderChanged(index, drugOrder, initDrugOrders)) return DrugOrderRowStatus.changed;

    return DrugOrderRowStatus.none;
  }

  /** 更動資料時，根據不同目標對資料做的處理，
   * @param _id 選到的資料 id
   * @param target 要修改的目標
   */
  updateDrugOrder<T>(_id: string, target?: { key: keyof DrugOrder, value: T }) {
    this.#drugOrders.update((orders) =>
      orders.map((x, y) => {
        if (x._id !== _id) return x;

        if (!target) return { ...this.#initDrugOrders[y] };

        return { ...x, [target.key]: target.value };
      }));
  }

  deleteProcess(_id: string) {
    // 新增的資料，如果是立即依然還可以刪掉
    const index = this.#drugOrders().findIndex((x) => x._id === _id);

    if (index >= this.#initDrugOrders.length) {
      this.#drugOrders.update((x) => x.filter((y) => y._id !== _id));
      return;
    }

    // 因有新增資料存在必須擺在第二位，已開立的資料如果立即就不能刪
    if (this.#initDrugOrders[index].frequency.code === 'STAT') return;

    const value = { code: OpdDrugOrderStatus.invalid, display: '作廢未生效' };
    this.updateDrugOrder(this.selectedId, { key: 'status', value });
  }

  undoProcess(_id: string) {

    const value = { code: OpdDrugOrderStatus.valid, display: '使用中' };
    this.updateDrugOrder(_id, { key: 'status', value });
  }

  recoverProcess(_id: string) {

    const index = this.#drugOrders().findIndex((x) => x._id === _id);

    // 還原id 順序不可換
    this.updateDrugOrder(_id);

    // 把編輯後新增的那筆初始資料刪掉
    // this.#drugOrders.update(x => x.filter(y => y._id !== this.#initDrugOrders[index]._id || y.endTime.isDefaultTime()));
  }

  /** 當編輯一筆資料之後，需要將對應的初始資料加入
   * @param _id 選到的資料 id
   */
  appendOriginalOrder(_id: string) {

    // 如果超出 backup 的長度，代表是新增的，不用處理
    const index = this.#drugOrders().findIndex((x) => x._id === _id);

    if (index >= this.#initDrugOrders.length) return;

    // 有編輯的話，改變 _id 以及 updateAt
    this.updateDrugOrder(_id, { key: 'updatedAt', value: this.#updateTime });
    this.updateDrugOrder(_id, { key: '_id', value: crypto.randomUUID() });

    // 如果初始資料的 id 有找到，代表已經加過了不用再加入了 (同一筆資料編輯第二次以上)
    const foundOrder = this.#drugOrders().find((x) => x._id === this.#initDrugOrders[index]._id);

    if (foundOrder) return;

    // 設定 endTime，加入初始資料
    const order = { ...this.#initDrugOrders[index], endTime: this.#updateTime };
    this.#drugOrders.update((x) => [...x, order]);
  }


  /** 透過 frequency.code 取得該欄位 medFreqTime 進行總量計算
   * @param order 傳入 focusedDrugOrder
   * @param medFreqTime 搜尋 focusedDrugOrder 的頻率時間數值
   * @return totalQty 計算完之總量 如果分母為0會導致計算錯誤 則直接回傳 0
   * @return 如果有搜尋到對應頻率則取得該欄位頻率進行總量計算
   */
  #getTotalQty(order: DrugOrder) {

    const result = { ...order.total };
    const frequencyCode = order.frequency.code;
    const medFreqTime = this.#medFreqTime.find((item) => item.frequency.code === frequencyCode);

    if (!medFreqTime) return result;

    if (medFreqTime.denominator === 0) return result;

    const dosage = order.dosage.value;
    const usedNum = medFreqTime.timePoints.length;
    const { molecular, denominator } = medFreqTime;

    const totalQty = (molecular / denominator) * usedNum * dosage * order.usedDays;

    result.value = totalQty;
    if (totalQty % 1 === 0) return result;

    result.value = parseFloat(totalQty.toFixed(2));
    return result;
  }

  #getUnitItem(title: string[]) {
    return title.map((x) => ({ label: x, command: () => this.#onUpdateUnit(x) }));
  }

  #onUpdateUnit(unit: string) {

    const order = this.#drugOrders().find((x) => x._id === this.selectedId);
    if (!order) return;

    let value = { ...order.dosage, code: unit, unit: unit };
    this.updateDrugOrder(this.selectedId, { key: 'dosage', value });

    value = { ...order.total, code: unit, unit: unit };
    this.updateDrugOrder(this.selectedId, { key: 'total', value });

    this.appendOriginalOrder(this.selectedId);
  }

  #onUpdateDays(value: number) {

    this.updateDrugOrder(this.selectedId, { key: 'usedDays', value });

    const order = this.#drugOrders().find((x) => x._id === this.selectedId);

    if (!order) return;

    const totalQty = this.#getTotalQty(order); // 根據劑量、頻率、天數計算出總量

    if (!totalQty) return;

    this.updateDrugOrder(this.selectedId, { key: 'total', value: totalQty });

    this.appendOriginalOrder(this.selectedId);
  }

  #isOrderChanged(index: number, drugOrder: DrugOrder, initDrugOrders: DrugOrder[]) {
    if (!initDrugOrders[index]) return;

    const backupWithoutStatus = this.#excludeProperty(initDrugOrders[index]);
    const rowDrugOrderWithoutStatus = this.#excludeProperty(drugOrder);

    return JSON.stringify(backupWithoutStatus) !== JSON.stringify(rowDrugOrderWithoutStatus);
  }

  #excludeProperty(drugOrder: DrugOrder) {
    const { status, ...newCopy } = drugOrder;

    return newCopy;
  }

}
