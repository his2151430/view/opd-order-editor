import { Pipe, PipeTransform, inject } from "@angular/core";
import { DrugOrderEditorService } from "./drug-order-editor.service";
import { DrugOrder } from "@his-viewmodel/med-order";
import { DrugOrderRowStatus } from "./opd-drug-order.enum";

@Pipe({ name: 'getRowCss', standalone: true })
export class GetRowCssPipe implements PipeTransform {

  statusClasses!: Record<DrugOrderRowStatus, string>;
  #drugOrderEditorService= inject(DrugOrderEditorService);

  transform(index: number, initDrugOrders: DrugOrder[], drugOrder: DrugOrder[]) {

    this.statusClasses = {
      'none': '',
      'deleted': 'deleted disabled-event delete-line',
      'new': 'new',
      'changed': 'changed'
    };

    const status = this.#drugOrderEditorService.getStatus(index, drugOrder[index], initDrugOrders);

    return this.statusClasses[status];
  }
}
