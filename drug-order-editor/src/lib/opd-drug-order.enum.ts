/** 前端判斷用的狀態 */
export enum DrugOrderRowStatus {
  none = 'none',
  deleted = 'deleted',
  new = 'new',
  changed = 'changed',
}

/** 實際會儲存到資料庫的狀態 */
export enum OpdDrugOrderStatus{
  invalid = '00',
  valid = '20'
}

export enum MenuAction{
  none,
  delete,
  undo,
  recover
}

