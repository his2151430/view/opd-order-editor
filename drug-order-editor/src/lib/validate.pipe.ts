import { Pipe, PipeTransform, inject } from "@angular/core";
import { DrugOrder } from "@his-viewmodel/med-order";
import { OpdDrugOrderValidator } from "./opd-drug-order.validator";

@Pipe({ name: 'validate', standalone: true })
export class ValidatePipe implements PipeTransform {
  #drugOrderValidator: OpdDrugOrderValidator = inject(OpdDrugOrderValidator);

  transform(drugOrder: DrugOrder, key: keyof DrugOrder) {
    if (key === 'dosage') return this.#drugOrderValidator.validateDosage(drugOrder);
    if (key === 'frequency') return !this.#drugOrderValidator.validateTotalQty(drugOrder);
    return false;
  }
}
