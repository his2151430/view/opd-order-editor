import { Coding } from '@his-base/datatypes';
import { Component, EventEmitter, Input, Output, SimpleChanges, WritableSignal, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { MegaMenuModule } from 'primeng/megamenu';
import { MenuItem } from 'primeng/api';
import { MenubarModule } from 'primeng/menubar';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { MenuModule } from 'primeng/menu';
import { InputNumberModule } from 'primeng/inputnumber';
import { FormsModule } from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { ValidatePipe } from './validate.pipe';
import { GetRowCssPipe } from './get-row-css.pipe';
import { GetStatusMenuPipe } from './get-state-menu.pipe';
import { FilterOrdersPipe } from './filter-orders.pipe';
import { DrugOrder, MedRoute } from '@his-viewmodel/med-order';
import { Item, ItemsDialogComponent } from '@his-directive/items-dialog/dist/items-dialog';
import { SharedService } from '@his-base/shared/';
import { DrugOrderEditorService } from './drug-order-editor.service';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'his-drug-order-editor',
  standalone: true,
  imports: [CommonModule, TableModule, ButtonModule, CheckboxModule,
    FormsModule,
    DialogModule,
    MegaMenuModule,
    MenubarModule,
    ScrollPanelModule,
    MenuModule,
    InputNumberModule,
    ValidatePipe,
    GetRowCssPipe,
    GetStatusMenuPipe,
    FilterOrdersPipe,
    ItemsDialogComponent,
    TranslateModule
  ],
  templateUrl: './drug-order-editor.component.html',
  styleUrls: ['./drug-order-editor.component.scss'],
})
export class DrugOrderEditorComponent {
/** 藥品處方 **/
@Input() drugOrders!: WritableSignal<DrugOrder[]>;

/** 藥品處方備份 **/
@Input() initDrugOrders!: DrugOrder[];

/** table 可視畫面高度 **/
@Input() scrollHeight!: string;

/** 更新時間：抓進入畫面的時間 **/
@Input() updateTime!: Date;

/** 傳遞出要打開 sidemenu 的要求 **/
@Output() sideMenuVisible = new EventEmitter();

isSelectDialogVisible = false;
isEnjoinVisible = false;
selectedEnjoin = '';
selectItems!: Item[];
// selectItems: WritableSignal<Item[]> = signal([]);
selectItemTitle = '';
unitMenu!: MenuItem[];
numberMenu!: MenuItem[];

#selectedType = '';
#selectedId!: string;
#routes: Item[] = [];
#frequencys: Item[] = [];

#sharedService = inject(SharedService);
#drugOrderEditorService = inject(DrugOrderEditorService);
#translate: TranslateService = inject(TranslateService);

ngOnChanges({ initDrugOrders }: SimpleChanges): void {
  if (!initDrugOrders || !initDrugOrders.currentValue) return;

  this.#drugOrderEditorService.setDrugOrders(this.drugOrders, this.initDrugOrders);
}

ngOnInit() {
  this.#drugOrderEditorService.initial(this.updateTime);
  this.unitMenu = this.#drugOrderEditorService.getUnitMenu();
  this.numberMenu = this.#drugOrderEditorService.getNumberMenu();

}

selectRow(_id: string) {
  this.#drugOrderEditorService.selectRow(_id);
}

/** 顯示選單
 * @param {string} type
 * @param {string} _id
 * @param {Coding} medType
 * @memberof DrugOrderEditorComponent
 */
async onShowDialog(type: string, _id:string, medType: Coding) {
  this.#selectedId = _id;
  this.#selectedType = type;

  if (type === 'route') {
    this.#routes = await this.#drugOrderEditorService.getMedRoutes(new Coding({code: medType.code}));
    this.selectItems = this.#routes;
    console.log('this.route :>> ', this.selectItems);
    // this.selectItems.set(this.#routes);
    this.selectItemTitle = this.#translate.instant(`請輸入途徑`);
  }

  if (type === 'frequency') {
    this.#frequencys = await this.#drugOrderEditorService.getFreqTimes(new Coding({code: medType.code}));
    // this.selectItems.set(this.#frequencys);
    this.selectItems = this.#frequencys;
    console.log('this.frequency :>> ', this.#frequencys);
    this.selectItemTitle = this.#translate.instant(`請輸入頻率`);
  }

  this.isSelectDialogVisible = !this.isSelectDialogVisible;
}

onEditEnjoin(_id: string) {

  this.#selectedId = _id;
  this.isEnjoinVisible = !this.isEnjoinVisible;

  const selectedOrder = this.drugOrders().find((x) => x._id === this.#selectedId);

  if (!selectedOrder) return;

  this.selectedEnjoin = selectedOrder.advise;
}

cancelEnjoinEdit() {

  this.isEnjoinVisible = false;
}

confirmEnjoinEdit() {

  this.#drugOrderEditorService.updateDrugOrder(this.#selectedId, { key: 'advise', value: this.selectedEnjoin });
  this.#drugOrderEditorService.appendOriginalOrder(this.#selectedId);
  this.isEnjoinVisible = false;
}

handleValueChanged(coding: Coding) {

  this.#drugOrderEditorService.handleValueChanged(coding, this.#selectedType, this.#selectedId);
}

/** 設置總量到 focusedOrder.total.value
 * @param {DrugOrder} drugOrder
 * @param {number} index
 */
setTotalQty(drugOrder: DrugOrder, index: number) {

  this.#drugOrderEditorService.setTotalQty(drugOrder, index);
}

onClickNew() {

  this.sideMenuVisible.emit(true);
  this.#drugOrderEditorService.onClickNew();
}

updateDosage(drugOrder: DrugOrder, index: number) {

  this.setTotalQty(drugOrder, index);
  this.#drugOrderEditorService.appendOriginalOrder(drugOrder._id);
}
}
