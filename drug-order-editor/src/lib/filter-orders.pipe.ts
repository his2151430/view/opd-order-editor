import { Pipe, PipeTransform } from "@angular/core";
import { DrugOrder } from '@his-viewmodel/med-order';
import '@his-base/date-extention';


@Pipe({ name: 'filterOrders', standalone: true })
export class FilterOrdersPipe implements PipeTransform {

  transform(initDrugOrders: DrugOrder[], drugOrders: DrugOrder[]) {

    return drugOrders.filter((x, y) => (x.endTime.isDefaultTime() || y < initDrugOrders.length));
  }
}
