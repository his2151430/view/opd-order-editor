import { Injectable } from '@angular/core';
import { mockMenuTitles, mockVisitDetail } from './mock-data';
import { MenuTitle } from '@his-directive/side-menu/dist/side-menu';
import { Observable, of } from 'rxjs';
import { VisitDetail } from './opd-order.interface';

@Injectable({
  providedIn: 'root'
})
export class OpdOrderEditorService {
  getMenuTitles(): Observable<MenuTitle[]> {
    // TODO: 跟後端串接資料
    return of(mockMenuTitles);
  }

  /** 取得目前的就診詳細資料
   * @returns
   */
  getVisitDetail(): Observable<VisitDetail> {
    // TODO: 需要監聽病患切換的事件
    // TODO: 跟後端串接資料
    return of(mockVisitDetail);
  }
}
