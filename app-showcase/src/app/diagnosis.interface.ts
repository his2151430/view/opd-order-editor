import { Coding } from '@his-base/datatypes';
export interface DiagItem extends Coding{
  displayEnglish: string;
  predictedValue?: number;
}
