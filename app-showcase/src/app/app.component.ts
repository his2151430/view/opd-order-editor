import { OpdOrderEditorService } from './opd-order-editor.service';
import { Component, WritableSignal, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { MenuTitle } from '@his-directive/side-menu/dist/side-menu';
import { MedRecord } from '@his-viewmodel/patient-info';
import { HeaderComponent } from '@his-directive/header/dist/header';
import { NhiRecordOptions } from './opd-order.interface';
import { DialogModule } from 'primeng/dialog';
import { FormsModule } from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { TabMenuModule } from 'primeng/tabmenu';
import { SideMenuComponent } from '@his-directive/side-menu/dist/side-menu';
import { TabViewModule } from 'primeng/tabview';
import { DividerModule } from 'primeng/divider';
import { SidebarModule } from 'primeng/sidebar';
import { MenuItem } from 'primeng/api';
import { SplitterModule } from 'primeng/splitter';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { DropdownModule } from 'primeng/dropdown';
import { SelectButtonModule } from 'primeng/selectbutton';
import { Coding } from '@his-base/datatypes';
import { ToolBarComponent } from '@his-directive/tool-bar/dist/tool-bar';
import { ExtandDialogComponent } from '@his-directive/extand-dialog/dist/extand-dialog';
import { InfoSidebarComponent } from '@his-directive/info-sidebar/dist/info-sidebar';
import { VisitDetail } from './opd-order.interface';
import { DrugOrderEditorComponent, DrugOrderEditorService } from '../../../drug-order-editor/src/public-api';
import { DrugOrder } from '@his-viewmodel/med-order/dist';
import { SharedService } from '@his-base/shared';
import { JetStreamWsService } from '@his-base/jetstream-ws';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import '@angular/localize/init';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, SplitterModule,
    ButtonModule,
    SplitButtonModule,
    DropdownModule,
    TabViewModule,
    DividerModule,
    SidebarModule,
    DialogModule,
    FormsModule,
    CheckboxModule,
    ScrollPanelModule,
    SelectButtonModule,
    ToggleButtonModule,
    TabMenuModule,
    TranslateModule,

    SideMenuComponent,
    InfoSidebarComponent,
    DrugOrderEditorComponent,
    ToolBarComponent,
    HeaderComponent
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  #translate: TranslateService = inject(TranslateService);
  selectOptions: any[]|undefined;
  selectValue: any;
  togBtnChecked: any;
  breadcurmb: string = this.#translate.instant(`門診醫囑系統>醫囑開立`);
  showTemplate: string = 'grid';
  menuTitles: MenuTitle[] = [];
  isSideMenuVisible = true;
  visitDetail!: VisitDetail;
  dittoItem!:MenuItem[];
  drugOrdersBackup!: DrugOrder[];
  signalDrugOrders: WritableSignal<DrugOrder[]> = signal([]);
  completeTime!:Date;

  sidebarExpanded = false;
  panelSizes = [83, 17]; // 初始為完全收合的尺寸

  // 連線網址
  #url = 'ws://10.251.42.49:8080';
  #user = '';
  #password = '';

  #sharedService = inject(SharedService);
  #opdOrderService = inject(OpdOrderEditorService);
  drugOrderEditorService = inject(DrugOrderEditorService);
  #jetStreamWsService = inject(JetStreamWsService);

  constructor() {

    this.#translate.setDefaultLang(`zh-Hant`);
  }

  async ngOnInit() {

    await this.connect();
    this.completeTime = new Date();

    this.#opdOrderService.getVisitDetail().subscribe((x) => {
      this.visitDetail = x;
    });
    this.drugOrdersBackup = structuredClone(this.visitDetail.drugOrders);
    this.#initOptions();
  }


  // 需放在 ngOnInit 啟動服務時就連線
  async connect() {

    await this.#jetStreamWsService.connect(this.#url, this.#user, this.#password);
  }

  async disconnect() {
    // 連線關閉前，會先將目前訂閱給排空
    await this.#jetStreamWsService.drain();
  }


  #initOptions() {
    this.#opdOrderService.getMenuTitles().subscribe((x) => {
      this.menuTitles = x;
    });
    this.signalDrugOrders.set(this.visitDetail.drugOrders);
    this.dittoItem = [
      {
        label: this.#translate.instant(`醫師上次`),
        command: () => {
          this.onDittoLastDoctor();
        },
      },
      {
        label: this.#translate.instant(`科別上次`),
        command: () => {
          this.onDittoLastDevision();
        },
      },
      {
        label: this.#translate.instant(`詳細選擇`),
        command: () => {
          this.onDittoDetailClick();
        },
      },
      { separator: true },
      {
        label: this.#translate.instant(`預設設定`),
        command: () => {
          this.onDittoConfigClick();
        },
      },
    ];

  }
  onDittoConfigClick() {
    throw new Error('Method not implemented.');
  }
  onDittoDetailClick() {
    throw new Error('Method not implemented.');
  }
  onDittoLastDevision() {
    throw new Error('Method not implemented.');
  }
  onDittoLastDoctor() {
    throw new Error('Method not implemented.');
  }
  onPrintClick() {
    throw new Error('Method not implemented.');
  }
  onRegisterClick() {
    throw new Error('Method not implemented.');
  }
  onCancelClick() {
    throw new Error('Method not implemented.');
  }
  onSaveClick() {
    throw new Error('Method not implemented.');
  }

  onChangeLayout() {
    throw new Error('Method not implemented.');
  }

  onShowInfoSidebar() {
    throw new Error('Method not implemented.');
  }
  onDittoClick() {
    throw new Error('Method not implemented.');
  }
  onChronicOrderClick() {
    throw new Error('Method not implemented.');
  }
}
